import { InfoAnim, SpriteSheetInfo } from "../interfaces/animation/info-anim";
import { AttackData } from 'src/app/interfaces/character/attack-data';

export interface CharacterInfo {
    name: string;
    sizeX: number;
    sizeY: number;
    columnN: number;
    iddleAnim: InfoAnim;
    attackAnim: InfoAnim;
    health: number;
    coldDown: number;
    damage: number;
    height: number;
    delayAttack: number;

}

export class Character {
    private name: string;
    private sizeX: number;
    private sizeY: number;
    private columnN: number;

    private iddleAnim: InfoAnim;
    private attackAnim: InfoAnim;
    private health: number;
    private coldDown: number;
    private damage: number;
    private currentHealth: number;
    private height: number;
    private delayAttack: number;

    private timeCd: number = 0;
    private readyToRemove: boolean = false;

    constructor(info: CharacterInfo) {
        this.name = info.name;
        this.sizeX = info.sizeX;
        this.sizeY = info.sizeY;
        this.columnN = info.columnN;

        this.iddleAnim = info.iddleAnim;
        this.attackAnim = info.attackAnim;
        this.health = info.health;
        this.currentHealth = info.health;
        this.coldDown = info.coldDown;
        this.damage = info.damage;
        this.height = info.height;
        this.delayAttack = info.delayAttack;
    }

    public GetIddle(): InfoAnim {
        return this.iddleAnim;
    }

    public GetSpriteSheetInfo(): SpriteSheetInfo {
        return {
            name: this.name,
            sizeX: this.sizeX,
            sizeY: this.sizeY,
            columnN: this.columnN
        }
    }

    public GetAttack(): InfoAnim {
        return this.attackAnim;
    }

    public IsPlayer(): boolean {
        return false;
    }

    public TakeDamage(damage: number): void {
        this.currentHealth -= damage;
        if (this.currentHealth <= 0) {
            this.Death();
        }
    }

    public Death(): void {
        this.currentHealth = 0;
        setTimeout(() => { this.readyToRemove = true }, 500);
    }

    public IsDead(): boolean {
        return (this.currentHealth <= 0);
    }

    public GetHeight(): number {
        return this.height;
    }

    public GetName(): string {
        return this.name;
    }

    public GetTotalHealth(): number {
        return this.health;
    }

    public GetCurrentHealth(): number {
        return this.currentHealth;
    }

    public GetColdDown(): number {
        return this.coldDown;
    }

    public ResolverCDBar(dt: number): boolean {
        let hasToAttack: boolean = false;
        this.timeCd += dt;
        if (this.timeCd >= this.coldDown) {
            this.timeCd = this.coldDown;

            hasToAttack = true;

            setTimeout(() => {
                this.timeCd = 0;
            });
        }

        return hasToAttack;

    }

    public GetCDBarStyle(): string {
        return this.timeCd / this.coldDown * 100 + "%";
    }

    public GetAttackData(): AttackData {
        return { damage: this.damage, isPlayer: this.IsPlayer(), delay: this.delayAttack };
    }

    public ReadyToRemove(): boolean {
        return this.readyToRemove;
    }

    public Remove(){
        this.readyToRemove = true;
    }



}