export enum EnumAnim {
    loop,
    oneShoot
};

export interface SpriteSheetInfo {
    name: string;
    sizeX: number;
    sizeY: number;
    columnN: number;
}

export interface InfoAnim {
    position: number;
    length: number;
    speed: number;
    play: EnumAnim;
}
