export interface AttackData {
    damage : number;
    isPlayer : boolean;
    delay: number;
}
