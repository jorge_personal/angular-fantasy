import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainMenuComponent } from './components/main-menu/main-menu.component';
import { SpriteAnimComponent } from './components/sprite-anim/sprite-anim.component';
import { IngameComponent } from './components/ingame/ingame.component';
import { CharacterComponent } from './components/character/character.component';
import { UiCharacterComponent } from './components/ui-character/ui-character.component';
import { CapitalizePipe } from './capitalize.pipe';
import { DamageTextComponent } from './components/damage-text/damage-text.component';

@NgModule({
  declarations: [
    AppComponent,
    MainMenuComponent,
    SpriteAnimComponent,
    IngameComponent,
    CharacterComponent,
    UiCharacterComponent,
    CapitalizePipe,
    DamageTextComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
