import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UiCharacterComponent } from './ui-character.component';

describe('UiCharacterComponent', () => {
  let component: UiCharacterComponent;
  let fixture: ComponentFixture<UiCharacterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UiCharacterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UiCharacterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
