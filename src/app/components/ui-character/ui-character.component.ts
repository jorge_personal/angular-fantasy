import { Component, Input, OnInit } from '@angular/core';
import { Character } from 'src/app/classes/character';

@Component({
  selector: 'app-ui-character',
  templateUrl: './ui-character.component.html',
  styleUrls: ['./ui-character.component.scss']
})
export class UiCharacterComponent implements OnInit {

  @Input() character: Character;

  constructor() { }

  ngOnInit() {
  }

  GetCD() {
    return {
      width: this.character.GetCDBarStyle()
      //transitionDuration: this.character.GetColdDown() + "s"
    }
  }

}
