import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-damage-text',
  templateUrl: './damage-text.component.html',
  styleUrls: ['./damage-text.component.scss']
})
export class DamageTextComponent implements OnInit {

  @Input() damage: number;

  constructor() { }

  ngOnInit() {
  }

}
