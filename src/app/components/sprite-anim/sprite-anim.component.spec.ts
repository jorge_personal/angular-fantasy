import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpriteAnimComponent } from './sprite-anim.component';

describe('SpriteAnimComponent', () => {
  let component: SpriteAnimComponent;
  let fixture: ComponentFixture<SpriteAnimComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpriteAnimComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpriteAnimComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
