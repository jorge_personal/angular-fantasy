import { Component, Input, OnInit } from '@angular/core';
import { InfoAnim, EnumAnim, SpriteSheetInfo } from '../../interfaces/animation/info-anim';

@Component({
  selector: 'app-sprite-anim',
  templateUrl: './sprite-anim.component.html',
  styleUrls: ['./sprite-anim.component.scss']
})
export class SpriteAnimComponent implements OnInit {

  @Input() spriteSheetInfo: SpriteSheetInfo;
  @Input() defaultAnim: InfoAnim;
  @Input() heightSprite: number;

  currentAnim: InfoAnim;
  indexFrame: number = 0;
  currentInterval: any;
  spriteSheet: string;
  trueHeightSprite: number;
  trueWidhtSprite: number;
  opacity: number = 0;

  constructor() { }

  ngOnInit() {
    this.SetSpriteSheet();
    this.PlayAnimation(this.defaultAnim);

  }

  ngAfterViewInit() {
    this.CalcSizes();
    this.opacity = 1;
  }

  ngOnDestroy() {
    this.ClearAnimInterval();
  }

  private SetSpriteSheet() {
    this.spriteSheet = "assets/sprites/" + this.spriteSheetInfo.name + ".png"
  };

  private CalcSizes() {
    this.trueHeightSprite = Math.trunc(document.documentElement.clientHeight * this.heightSprite / 100);
    let ratioSprite = this.trueHeightSprite / this.spriteSheetInfo.sizeY;
    this.trueWidhtSprite = this.spriteSheetInfo.sizeX * ratioSprite;
  }

  private PlayCurrentAnim() {
    this.ClearAnimInterval();
    this.indexFrame = 0;
    this.currentInterval = setInterval(() => { this.AnimInterval() }, this.currentAnim.speed);
  }

  private AnimInterval() {
    this.indexFrame++;
    if (this.indexFrame >= this.currentAnim.length) {
      switch (this.currentAnim.play) {
        case EnumAnim.loop:
          this.indexFrame = 0;
          break;
        case EnumAnim.oneShoot:
          this.PlayAnimation(this.defaultAnim);
          break;
      }
    }
  }

  private ClearAnimInterval() {
    if (this.currentInterval)
      clearInterval(this.currentInterval);
  }

  protected GetImgStyle() {

    return {
      top: this.indexFrame * -this.trueHeightSprite + "px",
      left: this.currentAnim.position * -this.trueWidhtSprite + "px",
      width: this.trueWidhtSprite * 2 + "px",
      height: this.trueHeightSprite * this.spriteSheetInfo.columnN + "px"
    }
  };

  protected GetSizeContainer() {

    return {
      width: this.trueWidhtSprite + "px",
      height: this.trueHeightSprite + "px",
      opacity: this.opacity
    }
  };

  public PlayAnimation(newAnim: InfoAnim) {
    this.currentAnim = Object.create(newAnim);
    this.PlayCurrentAnim();
  }

  public Dissapear(){
    this.opacity = 0; 
  }

}
