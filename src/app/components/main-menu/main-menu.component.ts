import { Component, OnInit } from '@angular/core';
import { EnumAnim, InfoAnim } from '../../interfaces/animation/info-anim';

@Component({
  selector: 'app-main-menu',
  templateUrl: './main-menu.component.html',
  styleUrls: ['./main-menu.component.scss']
})
export class MainMenuComponent implements OnInit {

  menuAnimation: InfoAnim;

  constructor() { }

  ngOnInit() {
    this.menuAnimation = {
      position: 0,
      length: 14,
      speed: 100,
      play: EnumAnim.loop
    }
  }

}
