import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { SpriteAnimComponent } from '../sprite-anim/sprite-anim.component';
import { InteractionEventsService } from 'src/app/services/interaction-events.service'
import { Character } from 'src/app/classes/character';


@Component({
  selector: 'app-character',
  templateUrl: './character.component.html',
  styleUrls: ['./character.component.scss']
})
export class CharacterComponent implements OnInit {

  @Input() character: Character;

  @ViewChild('mainAnim', { static: false }) mainAnim: SpriteAnimComponent;

  damagesQueued: number[] = [];

  damagesToShow: number[] = [];

  timeSinceDamageShow: number = 5;

  constructor(private interactionEventsService: InteractionEventsService) { }

  ngOnInit() {
  }

  private Attack(): void {
    this.mainAnim.PlayAnimation(this.character.GetAttack());
    this.interactionEventsService.DealDamage(this.character.GetAttackData());
  };

  private AddDamageToShow(damage: number) {
    this.damagesToShow.push(damage);
    this.damagesQueued.shift();

    setTimeout(() => {
      this.damagesToShow.shift();
    }, 3000);

  }

  private Dead() {
    this.mainAnim.Dissapear();
  }

  public TakeDamage(damage: number): void {
    this.character.TakeDamage(damage);
    this.damagesQueued.push(damage);

    if (this.character.IsDead()){
      this.Dead();
      this.AddDamageToShow(damage);

    }
  }

  public Update(dt: number): void {
    if (this.character.ResolverCDBar(dt))
      this.Attack();

    this.timeSinceDamageShow += dt;

    if (this.damagesQueued.length > 0 && this.timeSinceDamageShow > .5) {
      this.AddDamageToShow(this.damagesQueued[0])
      this.timeSinceDamageShow = 0;
    }
  }

  public IsDead(): boolean {
    return this.character.IsDead();
  }

  public Remove(){
    this.Dead();
    this.character.Remove();
  }




}
