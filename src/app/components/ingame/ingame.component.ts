import { Component, OnInit, QueryList, ViewChildren, ViewChild } from '@angular/core';
import { PlayerCharacter } from 'src/app/classes/player-character';
import { Character, CharacterInfo } from 'src/app/classes/character';
import { InteractionEventsService } from 'src/app/services/interaction-events.service'
import { AttackData } from 'src/app/interfaces/character/attack-data';

import * as dataWolf from 'src/assets/jsons/wolf.json';
import * as dataKnight from 'src/assets/jsons/knight.json';
import { from } from 'rxjs';
import { CharacterComponent } from '../character/character.component';

@Component({
  selector: 'app-ingame',
  templateUrl: './ingame.component.html',
  styleUrls: ['./ingame.component.scss']
})
export class IngameComponent implements OnInit {

  @ViewChildren(CharacterComponent) characters!: QueryList<CharacterComponent>;
  @ViewChildren('enemyComponent') enemyComponents!: QueryList<CharacterComponent>;

  @ViewChild('playerComponent', { static: false }) playerComponent: CharacterComponent;

  wolf: CharacterInfo = (dataWolf as any).default;
  knight: CharacterInfo = (dataKnight as any).default;

  player: PlayerCharacter;
  enemies: Character[] = [];

  elapsed: number;

  lastTime: number;

  restarting: boolean = false;

  constructor(private interactionEventsService: InteractionEventsService) { }

  ngOnInit() {

    this.interactionEventsService.damage.subscribe(
      (attackData: AttackData) => {
        if (attackData.isPlayer) {
            setTimeout(() => {
              const result = this.enemyComponents.toArray().filter(enemy => !enemy.IsDead());
              let enemyIndex: number = Math.floor(Math.random() * result.length);
              result[enemyIndex].TakeDamage(attackData.damage);
            }, attackData.delay);
        }
        else {
          setTimeout(() => {
            this.playerComponent.TakeDamage(attackData.damage);
          }, attackData.delay);

        }
      }

    )


    this.BasicSetup();

  }

  ngAfterViewInit() {
    window.requestAnimationFrame((timestamp) => { this.Update(timestamp) });
  }


  ngOnDestroy() {

  }

  private BasicSetup() {
    this.player = new PlayerCharacter(this.knight);

    this.enemies.push(
      new Character(this.wolf)
    );
    setTimeout(() => {
      this.enemies.push(
        new Character(this.wolf)
      );
    }, 5000)
  }

  private Update(timestamp: number) {

    if (this.lastTime === undefined)
      this.lastTime = timestamp;

    this.elapsed = (timestamp - this.lastTime) / 1000;

    this.enemies = this.enemies.filter(enemy => !enemy.ReadyToRemove());

    if (this.enemies.length) {
      this.characters.forEach((character: CharacterComponent) => {
        if (!character.IsDead())
          character.Update(this.elapsed);
      })
    }
    else {
      if (!this.restarting) {
        this.restarting = true;
        setTimeout(() => {
          this.Restart();
        }, 3000);
      }
    }

    this.lastTime = timestamp;

    window.requestAnimationFrame((timestamp) => { this.Update(timestamp) });

  }

  protected GridMonster(index: number): string {
    return "grid-monster-" + this.enemies.length + "-" + index;
  }

  private Restart(): void {
    this.playerComponent.Remove();
    setTimeout(() => {
      this.player = null;
    }, 500);
    setTimeout(() => {
      this.BasicSetup();
      this.restarting = false;
    }, 1500);
  }
}
