import { TestBed } from '@angular/core/testing';

import { InteractionEventsService } from './interaction-events.service';

describe('InteractionEventsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InteractionEventsService = TestBed.get(InteractionEventsService);
    expect(service).toBeTruthy();
  });
});
