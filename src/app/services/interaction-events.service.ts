import { EventEmitter, Injectable, Output } from '@angular/core';
import { AttackData } from 'src/app/interfaces/character/attack-data';

@Injectable({
  providedIn: 'root'
})
export class InteractionEventsService {
  @Output() damage: EventEmitter<any> = new EventEmitter();

  DealDamage(attackData: AttackData) {
    this.damage.emit(attackData);
  }

  constructor() { }
}
